import { Form, FormControl, Button } from 'react-bootstrap';
import React, { useState } from 'react';

import api from "../lib/api";

//Router
import {useHistory, useLocation} from "react-router-dom";

//Sous forme de Hooks
const SearchBar = ({ onResults }) => {
  const [typed, setTyped] = useState('');
  const history = useHistory();
  const location = useLocation();

  const search = async () => {
    console.log(location);
    if(location.hash === "#channels"){
      const resp = await api.get("/search",
        {
          params: {
            q: typed,
            part: 'snippet',
            maxResults: 10,
            type: 'channel'
          }
        });
      console.log('Received', resp.data.items);
      onResults(resp.data.items);
      history.push(`/channels/search/${typed}`);
    //}else if(location.hash === "#videos"){
    //Par défaut on recherche des vidéos
    }else{
      const resp = await api.get("/search",
      {
        params: {
          q: typed,
          part: 'snippet',
          maxResults: 10,
          type: 'video'
        }
      });
    console.log('Received', resp.data.items);
    onResults(resp.data.items);
    history.push(`/videos/search/${typed}`);
    }
  }

  return (
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" value={typed} onChange={e => setTyped(e.target.value)} />
      <Button variant="outline-info" onClick={search}>Search</Button>
    </Form>);
}

/*
// Sous forme de classe
class SearchBar extends React.Component{
  constructor(props){
    super(props);
    this.state = {typed: ''}
  }

  updateTyped = (event) => {
    this.setState({typed: event.target.value});
  }

  render(){
    return (
      <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" value={this.state.typed} onChange={this.updateTyped}/>
          <Button variant="outline-info">Search</Button>
      </Form>);
  }

}
*/

/*
//Sous forme de constante JS

const SearchBar = (props) => {
  return (
    <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-info">Search</Button>
    </Form>
);
}
*/

export default SearchBar;
