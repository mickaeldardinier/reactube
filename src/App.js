import './App.css';
import MyNav from './components/MyNav';
import { Container } from 'react-bootstrap';
import React, { useState } from 'react';
import Video from "./components/video/Video";
import DetailedVideo from "./components/video/DetailedVideo";
import Channel from "./components/channel/Channel"; 
import DetailedChannel from "./components/channel/DetailedChannel";

//Router
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const App = () => {
  const [items, setItems] = useState([]);
  const [selectedItem, selectItem] = useState(null);

  return (
    <Container className="p-3">
      <Router>
        <MyNav onResults={setItems} />

        <Switch>
          <Route path="/channels/search/:search">
            <>
              {items.map(i => {
                const {description, thumbnails, channelTitle, publishTime } = i.snippet;
                return (<Channel key={i.id.channelId} channelId={i.id.channelId} thumbnail={thumbnails.high}
                  description={description} channelTitle={channelTitle} publishTime={publishTime}
                   selectVideo={selectItem} />);
              })}
            </>
          </Route>

          <Route path="/channels/:channelId">
          {selectedItem != null && <DetailedChannel snippet={selectedItem.snippet} statistics={selectedItem.statistics} />
            }
          </Route>

          <Route path="/videos/search/:search">
            <>
              {items.map(i => {
                const { title, description, thumbnails, channelTitle, publishTime } = i.snippet;
                return (<Video key={i.id.videoId} videoId={i.id.videoId} thumbnail={thumbnails.high}
                  description={description} channelTitle={channelTitle} publishTime={publishTime} title={title}
                  selectVideo={selectItem} />);
              })}
            </>
          </Route>

          <Route path="/videos/:videoId">
            {selectedItem != null && <DetailedVideo player={selectedItem.player} snippet={selectedItem.snippet}
              statistics={selectedItem.statistics} />
            }
          </Route>

          <Route path="/">
            "Merci d'effectuer une recherche..."
          </Route>

        </Switch>
      </Router>
    </Container>
  );
}

export default App;
